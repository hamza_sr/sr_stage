<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demmandes', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('cin')->unique();
            $table->string('code');
            $table->string('cne')->unique();      
            $table->integer('nApogee')->nullable();
            $table->string('email')->nullable();
            $table->date('dateNaissance')->nullable();
            $table->string('description');
            $table->boolean('action')->default('0');
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demmandes');
    }
};
