<?php

namespace App\Http\Controllers;
use App\Models\Demmande;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use function PHPUnit\Framework\isEmpty;

class demandeController extends Controller
{
     
    public function demmande(Request $request){
        $demm = Demmande::where('action','!=',1)->get(); 
        return view ('pages.demmande',compact('demm'));  
    }
    public  function create (Request $request) {
        return view('pages.problem-bourse');

    } 
    public function create_ent(Request $request) {
        return view('pages.problem');

    }
    public function create_ent_g(Request $request) {
        return view('pages.problem-ent-g');

    }
    public function sherch_demmande(Request $request)
    {

        $cin = $request->input('cin');
        $students = Demmande::where('cin',$request->cin)->where('code',$request->code)->get();
        if($students->count() == 0){
            return redirect()->back()->with('erreur','Aucune Demande  Trouvee !');
        }
        return view('pages.search', compact('students', 'cin'));
    }
    //public  function sherch_demmande(Request $request) {
        //$data = $request->input( 'cin' );    
            //DB::table('demmandes') ->where('id','like', '%'.$data.'%')->get( ['id'] ) ;
        
   // }
    public function store(Request $request){

        $request->validate([
            'name' => 'required|string',
            'cin' => 'required|string',       
            'cne' => 'required|string',
            'nApogee' => 'required|integer',
            'email' => 'required|string',
            'description' => 'required|string',
        ]); 
        $code = Str::random(10);
        $demm = new demmande();
        $demm->name=$request->name;
        $demm->cin=$request->cin;
        $demm->cne=$request->cne;
        $demm->nApogee=$request->nApogee;
        $demm->email= $request->email;
        $demm->dateNaissance=$request->dateNaissance;
        $demm->description=$request->description;
        $demm->code=$code;
        
        $demm->save();
         
        //return redirect()->route("home");
      
        return redirect()->back()->with('status',"La demmande a été ajoutée avec succès.Votre code de confirmation est :")->with('code',$code);
        //return redirect()->route('demande', ['id' => $demmandes->
    }
    public function delete_demmande($id){
        $demm = Demmande::find(($id));
        $demm->delete();
        return redirect()->back()->with('status',"La demmande a été supprime avec succès.");

    }
     public function validation($id){
         $demm = Demmande::find(($id));
         $demm->action=true;
         $demm->save();
         return redirect()->back()->with('status',"La demmande a été traite avec succès.");

    }
}

