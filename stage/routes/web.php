<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\demandeController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::view('/','welcome');
Route::view('/problem','pages.problem');
Route::view('/problem-bourse','pages.problem-bourse');
Route::view('/problem-ent-g','pages.problem-ent-g');
Route::view('/suivi','pages.suivi');
Route::get('delete-demmande/{id}',[demandeController::class,'delete_demmande']);
Route::get('/sherch_demmande',[demandeController::class,'sherch_demmande'])->name('sherch_demmande');
Route::get('demmande',[demandeController::class,'demmande'])->middleware(["auth"]);
Route::post('/store_demmande',[demandeController::class,'create'])->name( 'store_demmande' ); 
Route::post('/store_demmande',[demandeController::class,'store'])->name( 'store_demmande' );
Route::post('/store_ent',[demandeController::class,'create_ent'])->name( 'store_ent' ); 
Route::post('/store_ent',[demandeController::class,'store'])->name( 'store_ent' );
Route::post('/store_ent_g',[demandeController::class,'create_ent_g'])->name('store_ent_g'); 
Route::post('/store_ent_g',[demandeController::class,'store'])->name( 'store_ent_g' );
Route::get('validate/{id}',[demandeController::class,'validation'])->middleware(["auth"]);
Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
