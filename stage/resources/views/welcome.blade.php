<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accueil</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <style>
 

 

a {
  text-decoration:none;
  color: #fff;
}
body{
  background-color: #f0f0f0;
}
footer {
    text-align: center;
    padding: 20px;
    background-color: #f0f0f0;
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
  }
.tt{
  color: #1d2de1
  
}
    </style>
</head>
<body>
  

  <nav id="navbar-example2" class="navbar  bg-white shadow-sm px-1 mb-5">
    <img src="images/tt.png"  alt="tt" width="110" class="rounded-8">
    
    <ul class="nav nav-pills">
      <li class="nav-item">
        <a class="nav-link" href="/">Accueil</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link " data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Problème de Ent</a>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="problem">Mot de passe oublié</a></li>
          <li><hr class="dropdown-divider"></li>
          <li><a class="dropdown-item" href="problem-ent-g">Problème général</a></li>
        </ul>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="problem-bourse">Problème de bourse</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="suivi">Suivre Mes Demmandes</a>
      </li>
      
      
    </ul>
  </nav>
   
   <section>
    <div class="container ">
        <div class="d-flex align-items-center justify-content-center p-5 ">
            <div class="col-6">
                <div class="d-grid gap-4">
                  <div class="btn-group" role="group">
                    <button type="button" class=" button-submit btn bg-info text-white p-3 " data-bs-toggle="dropdown" aria-expanded="false">
                      Probleme de Ent
                    </button>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="problem">Mot de passe oublié</a></li>
                      <li><a class="dropdown-item" href="problem-ent-g">Problem générale</a></li>
                    </ul>
                  </div>
                    <button type="button" class="button-submit btn btn-info p-3"><a href="problem-bourse">Problem de bourse</a></button>
                    <button type="button" class="button-submit btn btn-info p-3 "><a href="suivi">Suivre Mes Demmandes</a></button>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="footer text-center">
 &copy; All Rights Reserved by universite ibn tofail. Designed and Developed by
  <a href="http://www.uit.ac.ma/" target="_blank" class="tt">SSI-UIT</a>.
</footer>
  
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

</body>
</html>
