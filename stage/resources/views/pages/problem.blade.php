<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mot de passe oublié</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <style>

  .modal__body {
  padding: 1rem 1rem;
}


.title {
  text-align: center;
  font-size: 2rem;
  margin-bottom: 20px;
  color: #1a202c;
}
  body{
  background-color: #f0f0f0;
}

.tt{
  color: #1d2de1
  
}
footer {
    text-align: center;
    padding: 20px;
    background-color: #f0f0f0;
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
  }


    </style>
</head>
<body>
  <nav id="navbar-example2" class="navbar  bg-white shadow-sm px-3 mb-3">
    <img src="images/tt.png"  alt="tt" width="110" class="rounded">
    
    <ul class="nav nav-pills">
      <li class="nav-item">
        <a class="nav-link" href="/">Accueil</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Problème de Ent</a>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="problem">Mot de passe oublié</a></li>
          <li><hr class="dropdown-divider"></li>
          <li><a class="dropdown-item" href="problem-ent-g">Problème général</a></li>
        </ul>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="problem-bourse">Problème de bourse</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="suivi">Suivre Mes Demmandes</a>
      </li>
      
      
    </ul>
  </nav>
    <section>
   
      
        <h1 class="title">Mot de passe oublié</h1>
        @if (session('status'))
        <div class="alert alert-success">
            {!! session('status') !!}
          <span id="code" >{!! session('code') !!}</span>
            <button id="copyButton" class="btn btn-link">Copy</button>
        </div>
    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
        <script>
            
            new ClipboardJS('#copyButton', {
                text: function(trigger) {
                    return document.querySelector('#code').textContent.trim();
                }
            });
            document.getElementById('copyButton').addEventListener('click', function() {
                var clipboard = new ClipboardJS('#copyButton');
                clipboard.on('success', function(e) {
                    alert('Code copied successfully!');
                    e.clearSelection();
                });
            });
        </script>
    @endif
   
        <form class="form container" action="store_ent" method="POST">
          @csrf
          <div class="modal__body">
            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingInput" placeholder="CIN" name="cin">
              <label for="floatingInput">CIN</label>
            </div>
            <div class="form-floating mb-3">
              <input type="text" class="form-control" id="floatingInput" placeholder="CNE" name="cne">
              <label for="floatingInput">CNE</label>
            </div>
            <div class=" col-md-12">
              <div class="form-floating">
              <textarea class="form-control" placeholder="Leave a comment here" name="description" id="floatingTextarea2" style="height: 100px"></textarea>
              <label for="floatingTextarea2">Description</label>
              </div><br>
            <div class="modal__footer">
              <button type="submit" class="btn btn-primary">Envoyer</button>            
            </div>
          </div>
        </form>
        
    
    </section>
    <footer class="footer text-center">
      &copy;All Rights Reserved by universite ibn tofail. Designed and Developed by
      <a href="http://www.uit.ac.ma/" target="_blank" class="tt ">SSI-UIT</a>
    </footer>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
     
</body>
</html>