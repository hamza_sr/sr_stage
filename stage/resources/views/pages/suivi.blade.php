<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Suivre Mes Demmandes</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <style>
.heading {
  font-weight: bold;
  font-size: 20px;
  text-align: center;
}
.newsletter-form {
  max-width: 600px;
  margin: 0 auto;
  padding: 20px;
  border: 2px solid #333;
  border-radius: 8px;
  background-color: #f7f7f7;
  font-family: Arial, sans-serif;
}
.newsletter-form h2 {
  margin-top: 0;
  color: #333;
  font-size: 24px;
}
.newsletter-form label {
  display: block;
  font-weight: bold;
  color: #666;
  margin-bottom: 8px;
  
}
.newsletter-form input[type="text"] {
  width: 50%;
  padding: 10px;
  margin-bottom: 10px;
  border: 1px solid #ccc;
  border-radius: 4px;
}
.newsletter-form button[type="submit"] {
  width:30%;
  padding: 10px;
  border: none;
  border-radius: 4px;
  background-color: hsl(211, 100%, 50%);
  color: white;
  font-weight: bold;
  cursor: pointer;
}
header .joj ul{
    display: flex;
    justify-content: space-between;
    align-items: center;
    background-color: hsl(211, 100%, 50%);
    color: white;
    padding-left: 70px;
    padding-right: 70px;
    height: 55px;
}
form{
  text-align: center
}

a {
  text-decoration: none;
  color: #fff;
}
footer {
    text-align: center;
    padding: 20px;
    background-color: white;
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
  }
.tt{
  color: #1d2de1
  
}
    </style>
</head>
<body>
  <nav id="navbar-example2" class="navbar  bg-white shadow-sm px-3 mb-3">
    <img src="images/tt.png"  alt="tt" width="110" class="rounded">
    
    <ul class="nav nav-pills">
      <li class="nav-item">
        <a class="nav-link" href="/">Accueil</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Problème de Ent</a>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="problem"> Mot de passe oublié</a></li>
          <li><hr class="dropdown-divider"></li>
          <li><a class="dropdown-item" href="problem-ent-g">Problème générale</a></li>
        </ul>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="problem-bourse">Problème de bourse</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="suivi">Suivre Mes Demmandes</a>
      </li>
      
      
    </ul>
  </nav>
     <div class="newsletter-form">
      @if ($msg = Session::get('erreur'))
      <div class="alert alert-danger" role="alert">
          {{ $msg }}
      </div>
      @endif
      <section>
    <p class="heading"> Merci de saisir votre code de suivi:</p>
    <form class="form" action="{{ route('sherch_demmande') }}" method="GET">
      <label for="cin">CIN:</label>
      <input required="" placeholder="Cin" name="cin" id="cin" type="text">
      <label for="code">Code de Suivi:</label>
      <input required="" placeholder="Code" name="code" id="code" type="text"> <br>
      <button type="submit">Recherche</button>
    </form>
  </div>
</section>
  <footer class="footer text-center">
    &copy; All Rights Reserved by universite ibn tofail. Designed and Developed by
    <a href="http://www.uit.ac.ma/" target="_blank" class="tt">SSI-UIT</a>.
  </footer>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

</body>
</html>