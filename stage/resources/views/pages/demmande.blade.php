@extends('layouts.app')
@section('content')
    <div class="container text-center">
        <div class="row">
            <div class="col s12">
                <h1>Liste des demmandes </h1>
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                 @endif
     <table class="table align-middle mb-0 bg-white">
        <thead class="bg-light">
            <tr>
                <th >Id</th>
                <th >Nom complet</th>
                <th >CIN</th>
                <th >CNE</th>
                <th >Date Naissance</th>
                <th >nApogee</th>
                <th >Email</th>
                <th >Description</th>
                <th class="mb-6">action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($demm as $item) 
            <tr>
                <td >{{$item->id}}</td>
                <td >{{$item->name}}</td>
                <td >{{$item->cin}}</td>
                <td >{{$item->cne}}</td>
                <td >{{$item->dateNaissance}}</td>
                <td >{{$item->nApogee}}</td>
                <td >{{$item->email}}</td>
                <td >{{$item->description}}</td>
                <td class="mb-6" > 
                    
                    <a  href="validate/{{$item->id}}" class="btn btn-link btn-rounded btn-sm fw-bold" data-mdb-ripple-color="dark">Termine</a>
                 
                    <a  href="/delete-demmande/{{$item->id}}"  class="btn btn-link btn-rounded btn-sm fw-bold" data-mdb-ripple-color="dark">supprimer</a>
                   
                </td>
            
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>

@endsection

