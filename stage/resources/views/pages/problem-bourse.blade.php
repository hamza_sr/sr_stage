<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Probleme de bourse</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <style>
 

  
  .modal__body {
  padding: 1rem 1rem;
}



.title {
  text-align: center;
  font-size: 2rem;
  margin-bottom: 20px;
  color: #1a202c;
  
}
body{
  background-color: #f0f0f0;
}
footer {
    text-align: center;
    padding: 20px;
    background-color: #f0f0f0;
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
  }
.tt{
  color: #1d2de1
  
}
    </style>
</head>
<body>
  
  <nav id="navbar-example2" class="navbar bg-white shadow-sm px-3 mb-3">
    <img src="images/tt.png"  alt="tt" width="110" class="rounded">
    
    <ul class="nav nav-pills">
      <li class="nav-item">
        <a class="nav-link" href="/">Accueil</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Problème de Ent</a>
        <ul class="dropdown-menu">
          <li><a class="dropdown-item" href="problem">Mot de passe oublié</a></li>
          <li><hr class="dropdown-divider"></li>
          <li><a class="dropdown-item" href="problem-ent-g">Problème générale</a></li>
        </ul>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="problem-bourse">Problème de bourse</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="suivi">Suivre Mes Demmandes</a>
      </li>
      
      
    </ul>
  </nav>
 
    <div class="">
        <h1 class="title">Problème de bourse</h1>
        
        @if (session('status'))
    <div class="alert alert-success">
        {!! session('status') !!}
      <span id="code" >{!! session('code') !!}</span>
        <button id="copyButton" class="btn btn-link">Copy</button>
    </div>
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
    <script>
        
        new ClipboardJS('#copyButton', {
            text: function(trigger) {
                return document.querySelector('#code').textContent.trim();
            }
        });
        document.getElementById('copyButton').addEventListener('click', function() {
            var clipboard = new ClipboardJS('#copyButton');
            clipboard.on('success', function(e) {
                alert('Code copied successfully!');
               
            });
        });
    </script>
@endif
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <section>
        <form class="form" action="store_demmande" method="POST">
          
          <div class="modal__body">

            <div class="input">
              @csrf
              <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div class="wow fadeInUp" data-wow-delay="0.3s">
                       
                            <div class="row g-3">
                             
                                <div class="col-md-6">
                                    <div class="form-floating">
                                       <input type="text" class="form-control" id="tel" name="name" placeholder="Nom">
                                        <label for="name">Nom </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" id="cin" name="cin" placeholder="cin">
                                        <label for="cin">CIN</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" name="cne" id="cne" placeholder="CNE">
                                        <label for="cne">CNE</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text"  class="form-control" name="nApogee" id="nApogee" placeholder="nApogee">
                                        <label for="nApogee">nApogee</label>
                                    </div>
                                </div>
              
                                <div class="col-md-6">
                                    <div class="form-floating">
                                       <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                        <label for="email">Email </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="date" class="form-control" id="date" name="dateNaissance" placeholder="dateNaissance">
                                        <label for="dateNaissance">date de Naissance</label>
                                    </div>
                                </div>
                                <div class=" col-md-12">
                                  <div class="form-floating">
                                  <textarea class="form-control" placeholder="Leave a comment here" name="description" id="floatingTextarea2" style="height: 100px"></textarea>
                                  <label for="floatingTextarea2">Description</label>
                                  </div>
                                </div>
                                <div class="col-12 text-center pt-5">
                                    <button class="btn btn-primary rounded-pill py-3 px-5" type="submit">envoyer</button>
                                </div>
                            </div>
            </div><br>
      
         
        </form>
      </div>
      </section>
      <footer class="footer text-center">
        &copy; All Rights Reserved by universite ibn tofail. Designed and Developed by
        <a href="http://www.uit.ac.ma/" target="_blank" class="tt">SSI-UIT</a>.
      </footer>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous">
      
      </script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>

</body>
</html>
