<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">   
<style>
    
  .modal__body {
  padding: 1rem 1rem;
}
body{
  background-color: #f0f0f0;
}

.tt{
  color: #1d2de1
  
}
footer {
    text-align: center;
    padding: 20px;
    background-color: #f0f0f0;
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
  }
</style>
</head>
<body>
    <nav id="navbar-example2" class="navbar  bg-white shadow-sm px-3 mb-3">
        <img src="images/tt.png"  alt="tt" width="110" class="rounded">
        
        <ul class="nav nav-pills">
          <li class="nav-item">
            <a class="nav-link" href="/">Accueil</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Problème de Ent</a>
            <ul class="dropdown-menu">
              <li><a class="dropdown-item" href="problem">Mot de passe oublié</a></li>
              <li><hr class="dropdown-divider"></li>
              <li><a class="dropdown-item" href="problem-ent-g">Problème générale</a></li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="problem-bourse">Problème de bourse</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="suivi">Suivre Mes Demmandes</a>
          </li>
          
          
        </ul>
      </nav>
  <section>
    <div class="container text-center">
        <div class="row">
            <div class="col s12">
                <h1>Mes demmandes :{{$cin}}</h1>
                <table class="table table-bordered table-hover align-middle mb-0 bg-white mt-5">
                    <thead class="bg-light">
                      <tr>
                        <th >Id</th>
                        <th >Nom complet</th>
                        <th >CIN</th>
                        <th >CNE</th>
                        <th >Code</th>
                        <th >Date Naissance</th>
                        <th >nApogee</th>
                        <th >Email</th>
                        <th>Description</th>
                        <th >statut</th>
                      </tr>
                    </thead>
    
        <tbody>
            @foreach ($students as $item) 
            <tr>
                <td >{{$item->id}}</td>
                <td >{{$item->name}}</td>
                <td >{{$item->cin}}</td>
                <td >{{$item->cne}}</td>
                <td >{{$item->code}}</td>
                <td >{{$item->dateNaissance}}</td>
                <td >{{$item->nApogee}}</td>
                <td >{{$item->email}}</td>
                <td >{{$item->description}}</td>
                <td >
                    <i class="fas fa-check text-success">{{$item->action?"été traité":" en cours"}}</i>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
</section> 
<footer class="footer text-center">
    &copy;All Rights Reserved by universite ibn tofail. Designed and Developed by
    <a href="http://www.uit.ac.ma/" target="_blank" class="tt">SSI-UIT</a>.
  </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>

</body>
</html>
